﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

// A custom attribute to allow 2 authors per method.
[AttributeUsage(AttributeTargets.Property)]
public class AuthorsAttribute : Attribute
{
    protected string _authorName1;
    protected string _authorName2;

    public AuthorsAttribute(string name1, string name2)
    {
        _authorName1 = name1;
        _authorName2 = name2;
    }

    public string AuthorName1
    {
        get { return _authorName1; }
        set { _authorName1 = value; }
    }

    public string AuthorName2
    {
        get { return _authorName2; }
        set { _authorName2 = value; }
    }


    public override bool Equals(Object obj)
    {
        AuthorsAttribute auth;
        return EqualsCastCheck(obj, out auth)
            && AuthorName1 == auth.AuthorName1
            && AuthorName2 == auth.AuthorName2;
    }

    bool EqualsCastCheck(object obj, out AuthorsAttribute auth)
    {
        auth = obj as AuthorsAttribute;
        return auth != null;
    }

    // Use the hash code of the string objects and xor them together. 
    public override int GetHashCode()
    {
        return _authorName1.GetHashCode() ^ _authorName2.GetHashCode();
    }

    public override bool Match(object obj)
    {
        AuthorsAttribute auth;
        return EqualsCastCheck(obj, out auth)
            && (
                AuthorName1 == auth.AuthorName1
                && AuthorName2 == auth.AuthorName2

                || AuthorName1 == auth.AuthorName2
                && AuthorName2 == auth.AuthorName1);
    }
}

// Add some authors to methods of a class. 
public class TestClass1
{
    [Authors("William Shakespeare", "Herman Melville")]
    public string Property1 { get; }

    [Authors("Leo Tolstoy", "John Milton")]
    public string Property2 { get; }
}

// Add authors to a second class's methods. 
public class TestClass2
{
    [Authors("William Shakespeare", "Herman Melville")]
    public string Property1 { get; }

    [Authors("Leo Tolstoy", "John Milton")]
    public string Property2 { get; }

    [Authors("William Shakespeare", "John Milton")]
    public string Property3 { get; }

    [Authors("John Milton", "Leo Tolstoy")]
    public string Property4 { get; }
}

class DemoClass
{
    static void Main(string[] args)
    {
        // Get the type for both classes to access their metadata.
        Type clsType1 = typeof(TestClass1);
        Type clsType2 = typeof(TestClass2);

        // Iterate through each property of the first class. 
        foreach (var property in clsType1.GetProperties())
        {
            // Check each method for the Authors attribute.
            var authAttr1 = property.GetCustomAttributes(typeof(AuthorsAttribute), false).Cast<AuthorsAttribute>().SingleOrDefault();
            if (authAttr1 != null)
            {
                // Display the authors.
                Console.WriteLine("{0}.{1} was authored by {2} and {3}.",
                                  clsType1.Name, property.Name, authAttr1.AuthorName1,
                                  authAttr1.AuthorName2);
                // Get matching properties from the second class. 
                foreach (var property2 in TypeDescriptor.GetProperties(clsType2, new [] { authAttr1, }).Cast<PropertyDescriptor>())
                {
                    var authAttr2 = (AuthorsAttribute)property2.Attributes[typeof(AuthorsAttribute)];
                    if (Equals(authAttr1, authAttr2))
                        Console.WriteLine("{0}.{1} was also authored by the same team.",
                                          clsType2.Name, property2.Name);
                    else
                        Console.WriteLine("{0}.{1} was also authored by the same team, but {2} is listed before {3}.",
                                          clsType2.Name, property2.Name, authAttr2.AuthorName1, authAttr2.AuthorName2);
                }
                Console.WriteLine();
            }
        }
    }
}
// The example displays the following output: 
//    TestClass1.Property1 was authored by William Shakespeare and Herman Melville. 
//    TestClass2.Property1 was also authored by the same team. 
//     
//    TestClass1.Property2 was authored by Leo Tolstoy and John Milton. 
//    TestClass2.Property2 was also authored by the same team. 
//    TestClass2.Property4 was also authored by the same team, but John Milton is listed before Leo Tolstoy.
